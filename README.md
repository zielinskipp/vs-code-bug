
# VSCodeBugReprex


```r
git clone https://gitlab.com/zielinskipp/vs-code-bug
cd vs-code-bug

docker build -t test-rstudio .
docker run -d -p 8787:8787 -e PASSWORD=rstudio -v $(pwd):/home/rstudio/VSCodeBugReprex --name vs-bug test-rstudio
```

While using VSCode with R6 classes we run into such a scenario, when test using
R6 classes are not recognized if there are more than one argument in a constructor.
In such cases the whole test file is not run.

1. Working test with one argument

![one-agrument](imgs/one-argument.png)


2. All test not recognized when one test has more than one argument.

![Alt text](imgs/two-arguments.png)

Test are passing when run in console.